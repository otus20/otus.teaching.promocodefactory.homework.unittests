﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
       private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
       private readonly PartnersController _partnersController;

       public SetPartnerPromoCodeLimitAsyncTests()
       {
          var fixture = new Fixture().Customize(new AutoMoqCustomization());
          _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
          _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
       }

       [Fact]
       public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFund()
       {
          // Arrange
          var partnerId = Guid.Parse("3646f97b-d460-4dca-894d-bc63c73b3e99");

          _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
             .ReturnsAsync((Partner)null);

          var request = new SetPartnerPromoCodeLimitRequest
          {
             EndDate = DateTime.Now.AddMonths(1),
             Limit = 100
          };

          // Act
          var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
          
          // Assert
          result.Should().BeAssignableTo<NotFoundResult>();
       }

       [Fact]
       public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
       {
          // Arrange
          var partnerId = Guid.Parse("3646f97b-d460-4dca-894d-bc63c73b3e99");

          var partner = new TestPartnerBuilder(partnerId)
             .SetNotActive()
             .Build();

          _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
             .ReturnsAsync(partner);

          var request = new SetPartnerPromoCodeLimitRequest
          {
             EndDate = DateTime.Now.AddMonths(1),
             Limit = 100
          };

          // Act
          var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
          
          // Assert
          result.Should().BeAssignableTo<BadRequestObjectResult>();
       }

       [Fact]
       public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ClearsNumberOfIssuedPromocodes()
       {
          // Arrange
          var partnerId = Guid.Parse("3646f97b-d460-4dca-894d-bc63c73b3e99");

          var partner = new TestPartnerBuilder(partnerId)
             .WithPromocodesCount(10)
             .WithPartnerLimit(100)
             .Build();

          _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
             .ReturnsAsync(partner);
          
          _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()))
             .Returns(Task.CompletedTask);

          var request = new SetPartnerPromoCodeLimitRequest
          {
             EndDate = DateTime.Now.AddMonths(1),
             Limit = 100
          };

          // Act
          await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

          // Assert
          partner.NumberIssuedPromoCodes.Should().Be(0);
          partner.PartnerLimits.First().CancelDate.Should().NotBeNull();
       }
       
       [Fact]
       public async void SetPartnerPromoCodeLimitAsync_PartnerHasNoActiveLimits_NotClearsNumberOfIssuedPromocodes()
       {
          // Arrange
          var partnerId = Guid.Parse("3646f97b-d460-4dca-894d-bc63c73b3e99");
          const int numberIssuedPromoCodes = 10;

          var partner = new TestPartnerBuilder(partnerId)
             .WithPromocodesCount(numberIssuedPromoCodes)
             .WithCancelledPartnerLimit(100, DateTime.Now.AddMonths(-1), DateTime.Now.AddDays(-1))
             .Build();

          _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
             .ReturnsAsync(partner);

          _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()))
             .Returns(Task.CompletedTask);

          var request = new SetPartnerPromoCodeLimitRequest
          {
             EndDate = DateTime.Now.AddMonths(1),
             Limit = 100
          };

          // Act
          await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

          // Assert
          partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
       }

       [Theory]
       [InlineData(0)]
       [InlineData(-1)]
       public async void SetPartnerPromoCodeLimitAsync_LimitLessThanZero_ReturnsBadRequest(int limit)
       {
          // Arrange
          var partnerId = Guid.Parse("3646f97b-d460-4dca-894d-bc63c73b3e99");

          var request = new SetPartnerPromoCodeLimitRequest
          {
             EndDate = DateTime.Now.AddMonths(1),
             Limit = limit
          };

          // Act
          var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
          
          // Assert
          result.Should().BeAssignableTo<BadRequestObjectResult>();
       }
       
       [Fact]
       public async void SetPartnerPromoCodeLimitAsync_NewLimit_SavesNewLimitToDb()
       {
          // Arrange
          var partnerId = Guid.Parse("3646f97b-d460-4dca-894d-bc63c73b3e99");
          const int numberIssuedPromoCodes = 10;

          var partner = new TestPartnerBuilder(partnerId)
             .WithPromocodesCount(numberIssuedPromoCodes)
             .WithCancelledPartnerLimit(100, DateTime.Now.AddMonths(-1), DateTime.Now.AddDays(-1))
             .Build();

          _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
             .ReturnsAsync(partner);

          _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()))
             .Returns(Task.CompletedTask);

          var request = new SetPartnerPromoCodeLimitRequest
          {
             EndDate = DateTime.Now.AddMonths(1),
             Limit = 100
          };

          // Act
          await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

          // Assert
          _partnersRepositoryMock.Verify(
             repo => repo.UpdateAsync(It.Is<Partner>(p => IsPartnerUpdatedWithLimit(p, request))),
             Times.Exactly(1));
       }

       private bool IsPartnerUpdatedWithLimit(Partner actualPartner, SetPartnerPromoCodeLimitRequest request)
       {
          return actualPartner.PartnerLimits.Count(x =>
             x.Limit == request.Limit && x.EndDate == request.EndDate) == 1;
       }
    }
}