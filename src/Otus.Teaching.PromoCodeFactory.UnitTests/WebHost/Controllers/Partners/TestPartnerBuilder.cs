﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
   public class TestPartnerBuilder
   {
      private Partner _partner;

      public TestPartnerBuilder(Guid partnerId)
      {
         _partner = new Partner
         {
            Id = partnerId,
            IsActive = true,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
         };
      }

      public TestPartnerBuilder SetNotActive()
      {
         _partner.IsActive = false;
         return this;
      }

      public TestPartnerBuilder WithPromocodesCount(int issuedPromocodesCount)
      {
         _partner.NumberIssuedPromoCodes = issuedPromocodesCount;
         return this;
      }

      public TestPartnerBuilder WithPartnerLimit(int limit)
      {
         _partner.PartnerLimits.Add(
            new PartnerPromoCodeLimit
            {
               PartnerId = _partner.Id,
               CreateDate = DateTime.Now,
               Limit = limit
            });

         return this;
      }
      
      public TestPartnerBuilder WithCancelledPartnerLimit(int limit, DateTime createDate, DateTime cancelledDate)
      {
         _partner.PartnerLimits.Add(
            new PartnerPromoCodeLimit
            {
               PartnerId = _partner.Id,
               Limit = limit,
               CreateDate = createDate,
               CancelDate = cancelledDate
            });

         return this;
      }

      public Partner Build()
      {
         return _partner;
      }
   }
}